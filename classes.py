import requests  # Api calls modules
from templates import *  # templates module to import json format
import urllib3  # needed to silence the ssl warning

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)  # silencing the ssl warnings


class Harbor:

    def __init__(self, harbor, password, project, aduser, adgroup, adgroupdn, sslverify, user):
        """
        Constructor to create the harbor object and verify the connection

        :param harbor:
        :param password:
        :param project:
        :param aduser:
        :param adgroup:
        :param adgroupdn:
        :param sslverify:
        :param user:
        """
        print(f'Creating object for {harbor}')
        self.user = user
        self.name: str = harbor
        self.__password: str = password
        self.project: str = project
        self.aduser: str = aduser
        self.adgroup: str = adgroup
        self.adgroupdn: str = adgroupdn
        self.sslVerify: bool = sslverify

        # Verification of harbor liveness by testing the health api uri
        print(f'Verifying Harbor liveliness on {self.name}')
        # verify if harbor connection is up, and the url is correct.
        r = requests.get(f'https://{self.name}/api/v2.0/health',
                         auth=(self.user, self.__password), verify=self.sslVerify)
        if r.status_code != 200:
            print(f'{self.name} is down')
            exit(1)

    def test_project_name(self) -> bool:
        """
        Mehthod to test if project name is already created
        :return:
        """

        r = requests.head(f'https://{self.name}/api/v2.0/projects?project_name={self.project}',
                          auth=(self.user, self.__password), verify=self.sslVerify)

        if r.status_code == 200:
            print(f'Project already exist in {self.name}')
        else:
            return True

    def create_project(self) -> None:
        """
        Method to create a project in harbor
        """
        projecttemplate['project_name'] = self.project
        r = requests.post(f'https://{self.name}/api/v2.0/projects', auth=(self.user, self.__password),
                          verify=self.sslVerify, json=projecttemplate)

        if r.status_code >= 200 or r.status_code <= 299:
            print(f'Project Created in {self.name}')
        else:
            print(f'Project failed to be created in {self.name} ')

    def add_user(self) -> None:
        """
        Method to add active directory users to harbor project

        """
        for user in self.aduser:
            form_user = usertemplate
            form_user['member_user']['username'] = user

            r = requests.post(f'https://{self.name}/api/v2.0/projects/{self.project}/members',
                              auth=(self.user, self.__password),
                              verify=self.sslVerify, json=form_user)

            if r.status_code >= 200 or r.status_code <= 299:
                print(f'User {user} added to project name {self.project} in {self.name}')
            else:
                print(f'Failed to add {user} to project name {self.project} in {self.name}')
        print(r.text)
        print('here')

    def add_adgroup(self) -> None:
        """
        Method to add active directory group to harbor project

        """
        group_exist = False

        r = requests.get(f'https://{self.name}/api/v2.0/usergroups',
                         auth=(self.user, self.__password), verify=self.sslVerify)
        group_list = r.json()

        for current_group in group_list:
            if current_group['group_name'] == self.adgroup and current_group['ldap_group_dn'] == self.adgroupdn:
                print(f'Group {self.adgroup} exist in {self.name} adding it to project {self.project}')
                group_exist = True
                group_template = groupexisttemplate
                group_template['member_group']['group_name'] = self.adgroup
                group_template['member_group']['ldap_group_dn'] = self.adgroupdn
                group_template['member_group']['id'] = int(current_group['id'])

                r = requests.post(f'https://{self.name}/api/v2.0/projects/{self.project}/members',
                                  auth=(self.user, self.__password),
                                  verify=self.sslVerify, json=group_template)

        if not group_exist:
            print(f'Group {self.adgroup} do not exist in {self.name} adding it to project {self.project}')
            group_template = groupnoexisttemplate
            group_template['member_group']['ldap_group_dn'] = self.adgroupdn
            r = requests.post(f'https://{self.name}/api/v2.0/projects/{self.project}/members',
                              auth=(self.user, self.__password),
                              verify=self.sslVerify, json=group_template)
