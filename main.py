#!/usr/bin/python
import time

from classes import *  # import statement needed to gain access to Harbor Class
import sys
import getopt

harbor1: str = ''
harbor2: str = ''
password1: str = ''
password2: str = ''
project: str = ''
aduser: list
adgroup: str = ''
adgroupdn: str = ''
sslVerify: bool = True
user: str = 'admin'


def arguments(argv):
    global harbor1
    global harbor2
    global password1
    global password2
    global project
    global aduser
    global adgroup
    global sslVerify
    global user
    global adgroupdn

    try:
        opts, args = getopt.getopt(argv, "ha:b:c:d:e:f:g:i:j:k:",
                                   ["harbor1=", "harbor2=", 'password1=', 'password2=', 'project=', 'aduser=',
                                    'adgroup=', 'no-ssl-verify', 'user=', 'adgroup-dn='])
    except getopt.GetoptError:
        print('error')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('error')
            sys.exit()
        elif opt in ("-a", "--harbor1"):
            harbor1 = arg
        elif opt in ("-b", "--harbor2"):
            harbor2 = arg
        elif opt in ("-c", "--password1"):
            password1 = arg
        elif opt in ("-d", "--password2"):
            password2 = arg
        elif opt in ("-e", "--project"):
            project = arg
        elif opt in ("-f", "--aduser"):
            aduser = arg
            aduser = aduser.split(',')
        elif opt in ("-g", "--adgroup"):
            adgroup = arg
        elif opt in ("-i", "--no-ssl-verify"):
            sslVerify = False
        elif opt in ("-j", "--user"):
            user = arg
        elif opt in ("-k", "--adgroup-dn"):
            adgroupdn = arg


def main(argv):
    arguments(argv)

    harborInstant1 = Harbor(harbor1, password1, project, aduser, adgroup, adgroupdn, sslVerify, user)
    harborInstant2 = Harbor(harbor2, password2, project, aduser, adgroup, adgroupdn, sslVerify, user)

    if harborInstant1.test_project_name():
        harborInstant1.create_project()

    if harborInstant2.test_project_name():
        harborInstant2.create_project()

    time.sleep(10)

    harborInstant1.add_user()
    harborInstant2.add_user()

    harborInstant1.add_adgroup()
    harborInstant2.add_adgroup()


if __name__ == "__main__":
    main(sys.argv[1:])
