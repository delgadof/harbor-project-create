groupexisttemplate = {
    "role_id": 4,
    "member_group": {
        "group_name": "string",
        "ldap_group_dn": "string",
        "group_type": 1,
        "id": 0
    }
}

groupnoexisttemplate = {
    "role_id": 4,
    "member_group": {
        "ldap_group_dn": "string",
        "group_type": 1
    }
}

projecttemplate = {
    "project_name": "test",
    "metadata": {
        "public": "false"
    }
}

usertemplate = {
    "role_id": 2,
    "member_user": {
        "username": "string"
    }
}
